import React from 'react'
import {StyleSheet, TouchableOpacity, Text, View} from 'react-native';
//Car
import { FontAwesome5 } from '@expo/vector-icons';
//Delete
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default function TodoItem({ pressHandler, item }) {
  return (
    <TouchableOpacity onPress={() => pressHandler(item.key)}>
      <View style={styles.item}>
        <MaterialCommunityIcons name="delete-restore" size={20} color="black" />
        <Text style={styles.textItem} >{item.text}</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  item: {
    padding: 16,
    marginTop: 16,
    borderColor: '#bbb',
    borderWidth: 1,
    borderStyle: "dashed",
    borderRadius: 1,
    borderRadius: 10,
    flexDirection: 'row'
  },
  textItem: {
      marginLeft: 15
  }
});