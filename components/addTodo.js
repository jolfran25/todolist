import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Button, Alert} from 'react-native';

export default function AddTodo({ prevTodos, setTodos }) {
  [text, setText] = useState('');

  const changeHandler = (val) => {
    setText(val);
  };

  const submitHandler = (text) => {
    if(text.length > 3){
      setText('');
      setTodos(prevTodos => {
        return [          
          ...prevTodos,
          { text, key: Math.random().toString() }
        ];
      });
    } else {
      Alert.alert('OOPS', 'Todo must be over 3 characters long', [
        {text: 'Understood', onPress: () => console.log('alert closed') }
      ]);
    }
  };

  return (
    <View>
      <TextInput 
        style={styles.input} 
        placeholder='new todo...'
        onChangeText={changeHandler} 
        value={text} 
      />
      <Button color='coral' onPress={() => submitHandler(text)} title='add todo' />
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    marginBottom: 10,
    paddingHorizontal: 8,
    paddingVertical: 6,
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
  },
});